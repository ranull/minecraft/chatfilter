package com.ranull.chatfilter;

import com.ranull.chatfilter.commands.ChatFilterCommand;
import com.ranull.chatfilter.data.DataManager;
import com.ranull.chatfilter.events.Events;
import com.ranull.chatfilter.filter.Filter;
import com.ranull.chatfilter.filter.Spam;
import com.ranull.chatfilter.api.ChatFilterAPI;
import org.bukkit.plugin.java.JavaPlugin;

public final class ChatFilter extends JavaPlugin {
    public static ChatFilterAPI chatFilterAPI;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        DataManager dataManager = new DataManager(this);

        Filter filter = new Filter(this);
        Spam spam = new Spam();

        chatFilterAPI = new ChatFilterAPI(filter, spam, dataManager);

        getServer().getPluginManager().registerEvents(new Events(this, filter, dataManager, spam), this);

        getCommand("chatfilter").setExecutor(new ChatFilterCommand(this, dataManager, filter));
    }

    public ChatFilterAPI getAPI() {
        return chatFilterAPI;
    }
}
